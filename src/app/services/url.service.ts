import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Url } from '../models/url.model';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  constructor(private http: HttpClient) {

  }

  /**
   * Create tiny url
   */
  createUrl(originalUrl: string): Observable<Url> {
    return this.http.post<Url>(`/api/v1/url`, { originalUrl });
  }

  /**
   * Get original url
   */
  getOriginalUrl(shortUrl: string): Observable<Url> {
    return this.http.get<Url>(`/api/v1/url?shortUrl=${shortUrl}`);
  }

}
