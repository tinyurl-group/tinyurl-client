import { Routes, RouterModule } from '@angular/router';
import { TinyurlPageComponent } from './components/tinyurl-page/tinyurl-page.component';

const routes: Routes = [
  { path: '', component: TinyurlPageComponent },
  { path: '**', component: TinyurlPageComponent },
];

export const AppRoutes = RouterModule.forRoot(routes);
