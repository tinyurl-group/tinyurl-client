import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './components/app/app.component';
import { AppRoutes } from './app.routing';
import { MaterialModule } from './material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TinyurlPageComponent } from './components/tinyurl-page/tinyurl-page.component';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { UrlCardComponent } from './components/url-card/url-card.component';

@NgModule({
  imports: [
    BrowserModule,
    MaterialModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutes
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    UrlCardComponent,
    TinyurlPageComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
