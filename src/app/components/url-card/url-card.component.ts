import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-url-card',
  templateUrl: './url-card.component.html',
  styleUrls: ['./url-card.component.scss']
})
export class UrlCardComponent implements OnInit {
  @Input() title: string;
  @Input() url: string;
  constructor() { }

  ngOnInit() {
  }

}
