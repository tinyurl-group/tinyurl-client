import { Component, OnInit } from '@angular/core';
import { UrlService } from 'src/app/services/url.service';
import { Url } from 'src/app/models/url.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-tinyurl-page',
  templateUrl: './tinyurl-page.component.html',
  styleUrls: ['./tinyurl-page.component.scss']
})
export class TinyurlPageComponent implements OnInit {
  toShortUrl = true;
  error;
  url: string;
  constructor(private urlService: UrlService) { }

  ngOnInit() {
  }


  async createUrl(input) {
    const userUrl = input && input.value ? input.value : null;
    if (!userUrl) {
      return;
    }

    try {
      this.reset();
      const result = await (this.toShortUrl ?
        this.urlService.createUrl(userUrl).toPromise() :
        this.urlService.getOriginalUrl(userUrl).toPromise());
      this.url = this.toShortUrl ? `${environment.baseUrl}/${result.shortUrl}` : result.originalUrl;
      input.value = '';
    } catch (error) {
      this.error = error.error && error.error.message ? error.error.message : error.error;
      console.error(error);
    }
  }

  reset() {
    this.url = null;
    this.error = null;
  }

}
