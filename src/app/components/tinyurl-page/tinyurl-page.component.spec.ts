/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TinyurlPageComponent } from './tinyurl-page.component';

describe('TinyurlPageComponent', () => {
  let component: TinyurlPageComponent;
  let fixture: ComponentFixture<TinyurlPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TinyurlPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TinyurlPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
